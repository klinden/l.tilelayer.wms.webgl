declare namespace L {
  namespace tileLayer {
    namespace wms {
      function webGL(baseUrl: string, options?: WMSOptions): TileLayer.WMS.WebGL;
    }
  }
  namespace TileLayer {
    namespace WMS {
      class WebGL extends WMS {

      }
    }
  }
}
