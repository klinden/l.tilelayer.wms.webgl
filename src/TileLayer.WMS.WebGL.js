import * as L from 'leaflet';
import * as PIXI from 'pixi.js';
import * as Q from 'q';

(function() {
  L.TileLayer.WMS.WebGL = L.TileLayer.WMS.extend({
    onAdd: function(map) {
      // initialize PIXI canvas
      var size = map.getSize();
      this.renderer = new PIXI.autoDetectRenderer(size.x, size.y, { transparent: true });
      this.stage = new PIXI.Container();

      var isAnimated = this._map.options.zoomAnimation && L.Browser.any3d;
      L.DomUtil.addClass(this.renderer.view, 'pixi-renderer leaflet-zoom-' + (isAnimated ? 'animated' : 'hide'));

      // add the canvas to the DOM
      map._panes.overlayPane.appendChild(this.renderer.view);

      // call base method
      L.TileLayer.WMS.prototype.onAdd.apply(this, arguments);
    },

    createTile: function(coords) {
      var deferred = Q.defer();
      var tileUrl = this.getTileUrl(coords);
      var tileKey = this._tileCoordsToKey(coords);

      var tile = PIXI.Sprite.fromImage(this.getTileUrl(coords));
      tile.texture.baseTexture.on('loaded', function() { deferred.resolve(tile); });

      return deferred.promise;
    },

    _addTile: function(coords, container) {
      var self = this;
      var tilePos = this._getTilePos(coords),
          key = this._tileCoordsToKey(coords);

      var tilePromise = this.createTile(this._wrapCoords(coords));

      tilePromise.then(function(tile) {
        // TODO: set position
        tile.width = 256;
        tile.height = 256;
        tile.x = tilePos.x;
        tile.y = tilePos.y;

        // add the tile to the stage
        self.stage.addChild(tile);
        self.renderer.render(self.stage);

        // @event tileloadstart: TileEvent
        // Fired when a tile is requested and starts loading.
        // this.fire('tileloadstart', {
        //   tile: tile,
        //   coords: coords
        // });
      });
    }
  });

  L.tileLayer.wms.webGL = function (url, options) {
    return new L.TileLayer.WMS.WebGL(url, options);
  };
})();
