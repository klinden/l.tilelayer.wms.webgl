var path = require('path');

module.exports = {
  entry: './src/TileLayer.WMS.WebGL.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'l.tilelayer.wms.webgl.js'
  },
  externals: {
    leaflet: 'L',
    PIXI: 'PIXI'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'buble-loader',
      exclude: /node_modules/i
    }]
  }
};
